package com.dercd.bungeecord.plugins.connectedchat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.google.gson.JsonObject;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class PokeHandler
{
	protected Map<String, PlayerData> pData = new HashMap<String, PlayerData>();
	protected Map<UUID, Boolean> vanish = new HashMap<UUID, Boolean>();
	protected Map<UUID, Set<UUID>> sVanish = new HashMap<UUID, Set<UUID>>();
	protected Map<UUID, Set<UUID>> suggestions = new HashMap<UUID, Set<UUID>>();
	NetworkManager networkManager;
	private Lock vanishLock = new ReentrantLock();
	Lock playerLock = new ReentrantLock();
	Lock sVanishLock = new ReentrantLock();
	Lock suggestionsLock = new ReentrantLock();
	BungeeScheduler s = BungeeCord.getInstance().getScheduler();
	
	public PokeHandler(NetworkManager networkManager, ConnectedChat cc)
	{
		this.networkManager = networkManager;
	}

	public PlayerData pokeForPlayerSync(UUID u, ServerInfo si)
	{
		this.playerLock.lock();
		try
		{
			String s = si.getName() + u.toString();
			if (!this.pData.containsKey(s)) 
			{
				JsonObject jo = new JsonObject();
				jo.addProperty("action", "PlayerInfo");
				jo.addProperty("uuid", u.toString());
				Connection c = this.networkManager.messageProcessor.connected.get(si);
				if(c == null)
				{
					this.playerLock.unlock();
					return null;
				}
				c.send(jo);
			}
			for (int i = 0; i < 50 && !this.pData.containsKey(s); i++)
				Thread.sleep(100);
			PlayerData back = this.pData.remove(s);
			this.playerLock.unlock();
			if(back == null)
				System.out.println(si.getName() + ": No response while poking for " + u.toString());
			return back;
		}
		catch (Exception x)
		{
			System.out.println("Error while poking playerData for '" + u + "'");
			x.printStackTrace();
			this.playerLock.unlock();
			return null;
		}
	}
	
	public boolean pokeForVanishSync(UUID player, UUID requester, ServerInfo si)
	{
		this.vanishLock.lock();
		try
		{
			JsonObject jo = new JsonObject();
			jo.addProperty("action", "Vanish");
			jo.addProperty("uuid", player.toString());
			jo.addProperty("requester", requester.toString());
			Connection c = this.networkManager.messageProcessor.connected.get(si);
			if(c == null)
			{
				this.vanishLock.unlock();
				return false;
			}
			c.send(jo);
			for (int i = 0; i < 100 && !this.vanish.containsKey(player); i++)
				Thread.sleep(20);
			Boolean back = this.vanish.remove(player);
			this.vanishLock.unlock();
			if(back == null)
				System.out.println(si.getName() + ": No response while poking vanish for '" + player + "'");
			return back == null ? false : back;
		}
		catch (Exception x)
		{
			System.out.println((si == null ? "" : si.getName() + ": ") + "Error while poking vanish for '" + player + "'");
			x.printStackTrace();
			this.vanishLock.unlock();
			return false;
		}
	}
	
	public Set<UUID> getVisiblesSync(UUID requester, ServerInfo si)
	{
		this.sVanishLock.lock();
		try
		{
			JsonObject jo = new JsonObject();
			jo.addProperty("action", "SVanish");
			jo.addProperty("requester", requester.toString());
			Connection c = this.networkManager.messageProcessor.connected.get(si);
			if(c == null)
			{
				this.sVanishLock.unlock();
				return null;
			}
			c.send(jo);
			for (int i = 0; i < 100 && !this.sVanish.containsKey(requester); i++)
				Thread.sleep(20);
			Set<UUID> back = this.sVanish.remove(requester);
			this.sVanishLock.unlock();
			if(back == null)
				System.out.println(si.getName() + ": No response while poking vanish for players on server '" + si.getName() + "' for '" + requester + "'");
			return back;
		}
		catch (Exception x)
		{
			System.out.println((si == null ? "" : si.getName() + ": ") + "Error while poking vanish for players on server '" + si.getName() + "' for '" + requester + "'");
			x.printStackTrace();
			this.sVanishLock.unlock();
			return null;
		}
	}
	
	public Set<UUID> getSuggestionsSync(UUID requester, ServerInfo si, String input)
	{
		this.suggestionsLock.lock();
		try
		{
			JsonObject jo = new JsonObject();
			jo.addProperty("action", "Suggestions");
			jo.addProperty("requester", requester.toString());
			jo.addProperty("input", input);
			Connection c = this.networkManager.messageProcessor.connected.get(si);
			if(c == null)
			{
				this.suggestionsLock.unlock();
				return null;
			}
			c.send(jo);
			for (int i = 0; i < 100 && !this.suggestions.containsKey(requester); i++)
				Thread.sleep(20);
			Set<UUID> back = this.suggestions.remove(requester);
			if(back == null)
			{
				System.out.println(si.getName() + ": No response while poking suggestions for players on server '" + si.getName() + "' for '" + requester + "' with input '" + input + "'");
				return new HashSet<UUID>();
			}
			return back;
		}
		catch (Exception x)
		{
			System.out.println((si == null ? "" : si.getName() + ": ") + "Error while poking suggestions for players on server '" + si.getName() + "' for '" + requester + "' with input '" + input + "'");
			x.printStackTrace();
			this.suggestionsLock.unlock();
			return null;
		}
	}
	
	
}
