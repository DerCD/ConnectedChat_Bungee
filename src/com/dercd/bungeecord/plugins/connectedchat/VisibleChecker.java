package com.dercd.bungeecord.plugins.connectedchat;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class VisibleChecker
{
	PokeHandler ph;

	public VisibleChecker(PokeHandler ph)
	{
		this.ph = ph;
	}

	public boolean check(ProxiedPlayer sender, ProxiedPlayer receiver)
	{
		if (sender.hasPermission("cd.cc.ignorevanish"))
			return true;
		boolean p1 = this.ph.pokeForVanishSync(sender.getUniqueId(), receiver.getUniqueId(), sender.getServer().getInfo());
		boolean p2 = this.ph.pokeForVanishSync(receiver.getUniqueId(), sender.getUniqueId(), receiver.getServer().getInfo());
		return (!p2 || (p1 && p2 && !this.ph.networkManager.serverData.get(receiver.getServer().getInfo()).isBVMDisabled()));
	}
}
