package com.dercd.bungeecord.plugins.connectedchat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import net.md_5.bungee.api.config.ServerInfo;

public class ServerData
{
	private Map<UUID, PlayerData> colorData;
	private String chatFormat;
	private String serverName;
	private ServerInfo server;
	private List<String> networkIDs;
	private Set<Network> networks = new HashSet<Network>();
	private PokeHandler ph;
	private boolean disabledBVM = false, 
					disabledDMB = false;
	private boolean chatTook = false,
					tabTook = false,
					cmdTook = false,
					fullTook = false;
	
	public ServerData(ServerInfo si, List<String> networkIDs, PokeHandler ph)
	{
		this.serverName = si.getName();
		this.colorData = new HashMap<UUID, PlayerData>();
		this.server = si;
		this.networkIDs = networkIDs;
		this.ph = ph;
		this.chatFormat = "<%1$s>: %2$s";
	}

	public ServerData(String chatFormat, ServerInfo si, List<String> networkIDs, PokeHandler ph)
	{
		this.serverName = si.getName();
		this.chatFormat = chatFormat;
		this.colorData = new HashMap<UUID, PlayerData>();
		this.server = si;
		this.networkIDs = networkIDs;
		this.ph = ph;
	}

	public List<String> getNetworkIDs()
	{
		return this.networkIDs;
	}
	public ServerInfo getServer()
	{
		return this.server;
	}
	public String getName()
	{
		return this.serverName;
	}
	public String getChatFormat()
	{
		return this.chatFormat;
	}
	public boolean isBVMDisabled()
	{
		return this.disabledBVM;
	}
	public boolean isDMBDisabled()
	{
		return this.disabledDMB;
	}

	public void setBVMDisabled(boolean b)
	{
		this.disabledBVM = b;
	}
	public void setDMBDisabled(boolean b)
	{
		this.disabledDMB = b;
	}
	
	public void addNetwork(Network network)
	{
		if(this.networks.contains(network)) return;
		this.networks.add(network);
		network.addServer(this);
	}
	public Set<Network> getNetworks()
	{
		return this.networks;
	}
	public void removeNetwork(Network network)
	{
		this.networks.remove(network);
		network.removeServer(this.getName());
	}
	
	public PlayerData getPlayer(UUID u)
	{
		PlayerData pd;
		if (!this.colorData.containsKey(u) || (pd = this.colorData.get(u)) == null)
		{
			pd = this.ph.pokeForPlayerSync(u, this.server);
			if (pd != null) putPlayer(u, pd);
		}
		return pd;
	}
	public void removePlayer(UUID u)
	{
		this.colorData.remove(u);
	}
	public boolean hasPlayer(UUID u)
	{
		return (this.colorData.containsKey(u) && this.colorData.get(u) != null);
	}
	public void putPlayer(UUID u, PlayerData pData)
	{
		this.colorData.put(u, pData);
	}
	public void putPlayer(UUID u, String prefix, String suffix)
	{
		this.colorData.put(u, new PlayerData(u, prefix, suffix));
	}
	public Set<Entry<UUID, PlayerData>> getPlayers()
	{
		return this.colorData.entrySet();
	}
	
	public boolean isChatTook()
	{
		return this.chatTook;
	}
	public boolean isTabTook()
	{
		return this.tabTook;
	}
	public boolean isCmdTook()
	{
		return this.cmdTook;
	}
	public void setChatTook(boolean b)
	{
		this.chatTook = b;
	}
	public void setTabTook(boolean b)
	{
		this.tabTook = b;
	}
	public void setCmdTook(boolean b)
	{
		this.cmdTook = b;
	}
	public boolean isTook()
	{
		return this.fullTook;
	}
	public void setTook(boolean b)
	{
		this.fullTook = b;
	}
	
	public void setChatFormat(String chatFormat)
	{
		this.chatFormat = chatFormat;
	}
}
