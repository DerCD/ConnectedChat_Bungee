package com.dercd.bungeecord.plugins.connectedchat;

import java.io.IOException;
import java.net.Socket;
import com.google.gson.JsonObject;

import com.dercd.cdio.CDIncommingConnection;
import net.md_5.bungee.api.config.ServerInfo;

public class Connection extends CDIncommingConnection
{
	Socket socket;
	MessageProcessor mp;
	protected ServerInfo si;
	public static final String keyWord = "QVVuaWNvcm5IYXMxSG9ybg==";
	
	public Connection(Socket socket, MessageProcessor mp) throws Exception
	{
		super(socket);
		this.name = "ConnectedChat_BC";
		setKeyWordIn(keyWord.getBytes());
		this.socket = socket;
		this.mp = mp;
		this.out = (s) -> { System.out.println("[ConnectedChat] " + s); };
		Connection self = this;
		this.onClosed = (cdic, reason) ->
		{
			this.mp.connected.removeOther(self);
			this.mp.cc.cm.silentSubscribes.removeKey(self);
			this.mp.cc.cm.ignoreSubscribes.removeKey(self);
			if(self.si != null)
				this.mp.networkManager.removeServer(self.si);
		};
		createProtocol();
		this.protocol.setDIO((s) ->
		{
			mp.addToQueue(s, Connection.this);
		});
	}
	
	public ServerInfo getServer()
	{
		return this.si;
	}
	
	public void send(JsonObject jo) throws IOException
	{
		if(jo == null)
			send((String) null);
		else
			send(jo.toString());
	}
}
