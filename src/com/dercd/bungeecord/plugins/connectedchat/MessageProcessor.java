package com.dercd.bungeecord.plugins.connectedchat;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dercd.bungeecord.cdlib.tools.Tools.MessageBuffer;
import com.dercd.bungeecord.cdlib.tools.collection.CDEntry;
import com.dercd.bungeecord.cdlib.tools.collection.SyncedMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import com.dercd.cdio.CloseReason;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class MessageProcessor implements Listener, Runnable
{
	PokeHandler ph;
	NetworkManager networkManager;
	BungeeScheduler s = BungeeCord.getInstance().getScheduler();
	ConnectedChat cc;
	ProfileManager pm;
	Lock queueLock = new ReentrantLock();
	public List<Entry<String, Connection>> queue = new ArrayList<Entry<String, Connection>>();
	protected SyncedMap<ServerInfo, Connection> connected = new SyncedMap<ServerInfo, Connection>();
	private JsonParser jsonParser = new JsonParser();
	
	public MessageProcessor(PokeHandler ph, NetworkManager networkManager)
	{
		this.ph = ph;
		this.networkManager = networkManager;
		this.cc = networkManager.chatManager.cc;
		this.pm = this.cc.pm;
	}

	public synchronized void addToQueue(String json, Connection c)
	{
		this.queueLock.lock();
		try
		{
			this.queue.add(new CDEntry<String, Connection>(json, c));
			notifyAll();
		}
		finally { this.queueLock.unlock(); }
	}

	public void process(String json, Connection c) throws IOException
	{
		JsonObject jo = (JsonObject) this.jsonParser.parse(json);
		switch (jo.get("action").getAsString())
		{
			case "PlayerInfo":
				procPlayerData(jo, c);
				break;
			case "Info":
				procServerData(jo, c);
				break;
			case "Vanish":
				procVanish(jo);
				break;
			case "GetPlayer":
				procGet(jo, c);
				break;
			case "TakePlayer":
				procTake(jo, c);
				break;
			case "RevokePlayer":
				procRevokePlayer(jo, c);
				break;
			case "GetServer":
				procServerDetach(jo, c);
				break;
			case "TakeServer":
				procServerAttach(jo, c);
				break;
			case "PlayerName":
				procPlayerName(jo, c);
				break;
			case "GetSilent":
				procGetSilent(jo, c);
				break;
			case "PlayerCommand":
				procPlayerCommand(jo);
				break;
			case "GetIgnorants":
				procGetIgnorants(jo, c);
				break;
			case "GetDIgnorants":
				procGetDIgnorants(jo, c);
				break;
			case "DisableBVM":
				procDisableBVM(c);
				break;
			case "EnableBVM":
				procEnableBVM(c);
				break;
			case "DisableDMB":
				procDisableDMB(jo, c);
				break;
			case "EnabledDMB":
				procEnableDMB(jo, c);
				break;
			case "DisableServerDMB":
				procDisableServerDMB(c);
				break;
			case "EnableServerDMB":
				procEnableServerDMB(c);
				break;
			case "Message":
				procMessage(jo, c);
				break;
			case "RequestPlayer":
				procRequestPlayer(jo, c);
				break;
			case "SVanish":
				procSVanish(jo);
				break;
			case "Suggestions":
				procSuggestions(jo);
				break;
		}
	}

	private void procPlayerCommand(JsonObject jo)
	{
		ProxiedPlayer pp = this.networkManager.bc.getPlayer(UUID.fromString(jo.get("uuid").getAsString()));
		this.networkManager.chatManager.addChat(new ChatEvent(pp, pp.getServer(), jo.get("data").getAsString()));
	}

	private void procGetSilent(JsonObject jo, Connection c) throws IOException
	{
		JsonObject send = new JsonObject();
		send.addProperty("action", "Silent");
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		PlayerProfile pp = this.pm.getProfile(u);
		send.addProperty("uuid", u.toString());
		send.addProperty("data", pp.isSilent());
		c.send(send);
		this.cc.cm.silentSubscribes.add(c, u);
	}
	private void procGetIgnorants(JsonObject jo, Connection c) throws IOException
	{
		JsonObject send = new JsonObject();
		send.addProperty("action", "PlayerIgnorants");
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		PlayerProfile pp = this.pm.getProfile(u);
		JsonArray ja = new JsonArray();
		for(UUID ignore : pp.getIgnores())
			ja.add(new JsonPrimitive(ignore.toString()));
		send.add("data", ja);
		send.addProperty("uuid", u.toString());
		c.send(send);
		this.networkManager.chatManager.ignoreSubscribes.add(c, u);
	}
	private void procGetDIgnorants(JsonObject jo, Connection c) throws IOException
	{
		JsonObject send = new JsonObject();
		send.addProperty("action", "PlayerDIgnorants");
		String name = jo.get("name").getAsString();
		JsonArray ja = new JsonArray();
		for(UUID ignore : this.networkManager.chatManager.playerDIgnores.getByVal(name))
			ja.add(new JsonPrimitive(ignore.toString()));
		send.add("data", ja);
		send.addProperty("name", name);
		c.send(send);
		this.networkManager.chatManager.dignoreSubscribes.add(c, name);
	}
	
	
	private void procPlayerName(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		if (sd == null) return;
		PlayerData pd = sd.getPlayer(u);
		pd.setDisplayName(jo.get("data").getAsString());
	}
	private void procRevokePlayer(JsonObject jo, Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		if (sd == null) return;
		sd.removePlayer(UUID.fromString(jo.get("uuid").getAsString()));
	}
	
	private void procServerDetach(JsonObject jo, Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		JsonArray ja = jo.get("types").getAsJsonArray();
		for(JsonElement je : ja)
		{
			switch(je.getAsString().toLowerCase())
			{
				case "chat":
					sd.setChatTook(true);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") detached chat-processing");
					break;
				case "cmd":
					sd.setCmdTook(true);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") detached cmd-processing");
					break;
				case "tab":
					sd.setTabTook(true);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") detached tab-completion");
					break;
				case "all":
					sd.setTook(true);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") detached entire server");
					break;
				default:
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") unknown detach-type '" + je.getAsString() + "'");
					break;
			}
		}
	}
	private void procServerAttach(JsonObject jo, Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		JsonArray ja = jo.get("types").getAsJsonArray();
		for(JsonElement je : ja)
		{
			switch(je.getAsString().toLowerCase())
			{
				case "chat":
					sd.setChatTook(false);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") attached chat-processing");
					break;
				case "cmd":
					sd.setCmdTook(false);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") attached cmd-processing");
					break;
				case "tab":
					sd.setTabTook(false);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") attached tab-completion");
					break;
				case "all":
					sd.setTook(false);
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") attached entire server");
					break;
				default:
					System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") unknown attach-type '" + je.getAsString() + "'");
					break;
			}
		}
	}
	private void procGet(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		PlayerData pd = sd.getPlayer(u);
		pd.setTook(true);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") detached " + u.toString());
	}
	private void procTake(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		PlayerData pd = sd.getPlayer(u);
		pd.setTook(false);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") attached " + u.toString());
	}
	
	private void procRequestPlayer(JsonObject jo, Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		if(sd == null) return;
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		new Thread(() -> { sd.getPlayer(u); }).start();
	}
	private void procPlayerData(JsonObject jo, Connection c)
	{
		UUID uuid = UUID.fromString(jo.get("uuid").getAsString());
		PlayerData pd = new PlayerData(uuid, jo.get("prefix").getAsString(), jo.get("suffix").getAsString());
		if (jo.has("displayName")) pd.setDisplayName(jo.get("displayName").getAsString());
		this.ph.pData.put(c.getServer().getName() + uuid.toString(), pd);
	}
	private void procServerData(JsonObject jo, Connection c) throws IOException
	{
		String serverID = jo.get("id").getAsString();
		ServerInfo si = this.networkManager.bc.getServerInfo(serverID);
		if(si == null)
		{
			System.err.println("ConnectedChat: " + c.getIPString() + ": Server '" + serverID + "' not found");
			c.close(CloseReason.MANUAL);
			return;
		}
		c.si = si;
		this.connected.put(si, c);
		String chatFormat = jo.get("chatFormat").getAsString();
		List<String> networkIDs = new ArrayList<String>();
		JsonArray nIDs = jo.get("networkIDs").getAsJsonArray();
		for(JsonElement je : nIDs)
			networkIDs.add(je.getAsString());
		ServerData sd = new ServerData(chatFormat, c.getServer(), networkIDs, this.ph);
		this.networkManager.removeServer(c.getServer());
		for (String nID : networkIDs)
		{
			if (!this.networkManager.networks.containsKey(nID) || this.networkManager.networks.get(nID) == null)
				this.networkManager.addNetwork(new Network(this.networkManager, nID));
			this.networkManager.networks.get(nID).addServer(sd);
			this.networkManager.serverData.put(c.getServer(), sd);
		}
		System.out.println("ConnectedChat: Connection from " + serverID + " (" + c.getIPString() + ") accepted");
		procAdditionalServerData(jo, c);
	}
	private void procAdditionalServerData(JsonObject jo, Connection c) throws IOException
	{
		JsonElement je = jo.get("additional");
		if(je == null) return;
		JsonArray additionalData = je.getAsJsonArray();
		for(JsonElement aje : additionalData)
			process(aje.toString(), c);
	}
	
	private void procDisableBVM(Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		sd.setBVMDisabled(true);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") disabled BVM");
	}
	private void procEnableBVM(Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		sd.setBVMDisabled(false);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") enabled BVM");
	}
	
	private void procMessage(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		String messageId = jo.get("data").getAsString();
		try
		{
			MessageBuffer.send(this.cc.getProxy().getPlayer(u), (String) Messages.class.getField(messageId).get(null));
		}
		catch(Exception x)
		{
		}
	}
	
	private void procDisableDMB(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		PlayerData pd = sd.getPlayer(u);
		pd.setDMBDisabled(true);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") disabled DMB for " + u.toString());
	}
	private void procEnableDMB(JsonObject jo, Connection c)
	{
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		PlayerData pd = sd.getPlayer(u);
		pd.setDMBDisabled(false);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") enabled DMB for " + u.toString());
	}
	private void procDisableServerDMB(Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		sd.setDMBDisabled(true);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") disabled DMB for the entire server");
	}
	private void procEnableServerDMB(Connection c)
	{
		ServerData sd = this.networkManager.serverData.get(c.getServer());
		sd.setDMBDisabled(false);
		System.out.println("ConnectedChat: " + c.getServer().getName() + " (" + c.getIPString() + ") enabled DMB for the entire server");
	}
	
	private void procVanish(JsonObject jo)
	{
		this.ph.vanish.put(UUID.fromString(jo.get("uuid").getAsString()), jo.get("data").getAsBoolean());
	}
	private void procSVanish(JsonObject jo)
	{
		Set<UUID> uuids = new HashSet<UUID>();
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		JsonArray ja = jo.get("data").getAsJsonArray();
		for(JsonElement je : ja)
			uuids.add(UUID.fromString(je.getAsString()));
		this.ph.sVanish.put(u, uuids);
	}
	
	private void procSuggestions(JsonObject jo)
	{
		Set<UUID> uuids = new HashSet<UUID>();
		UUID u = UUID.fromString(jo.get("uuid").getAsString());
		JsonArray ja = jo.get("data").getAsJsonArray();
		for(JsonElement je : ja)
			uuids.add(UUID.fromString(je.getAsString()));
		this.ph.suggestions.put(u, uuids);
	}
	
	@Override
	public void run()
	{
		try
		{
			Entry<String, Connection> e;
			while(true)
			{
				while(!this.queue.isEmpty())
				{
					this.queueLock.lock();
					e = this.queue.remove(0);
					this.queueLock.unlock();
					try
					{
						process(e.getKey(), e.getValue());
					}
					catch (Exception x)
					{
						printError(x, e);
					}
				}
				secWait();
			}
		}
		catch (InterruptedException ox)
		{
			System.out.print("PokeListener-Thread: Interrupted");
		}
	}
	private void printError(Exception x, Entry<String, Connection> e)
	{
		PrintStream out = System.err;;
		out.println("--------------------");
		out.println("ConnectedChat:PokeListener: " + x.getClass().getName());
		x.printStackTrace();
		out.println("Debug informations:");
		try
		{
			if(e == null)
				out.println("Entry<String, Connection>: null");
			else
			{
				out.println("Json: " + e.getKey());
				if(e.getValue() == null)
					out.println("Connection: null");
				else
					out.println("Server: " + e.getValue().getServer().getName());
			}
		}
		catch (Throwable t)
		{
			out.println("Error while printing debug infos");
			t.printStackTrace();
		}
		out.println("End debug informations");
		out.println("--------------------");
	}
	private synchronized void secWait() throws InterruptedException
	{
		if(!this.queue.isEmpty()) return;
		wait();
	}
}
