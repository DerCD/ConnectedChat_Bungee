package com.dercd.bungeecord.plugins.connectedchat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dercd.bungeecord.cdlib.CDLib;
import com.dercd.bungeecord.cdlib.exceptions.CDPlayerNameNotFoundException;
import com.dercd.bungeecord.cdlib.exceptions.CDPlayerResolveException;
import com.dercd.bungeecord.cdlib.tools.Tools.Data;
import com.dercd.bungeecord.cdlib.tools.Tools.MessageBuffer;
import com.dercd.bungeecord.cdlib.tools.Tools.Var;
import com.dercd.bungeecord.cdlib.tools.collection.SyncedSetMap;
import com.dercd.bungeecord.cdlib.tools.minecraft.NameFetcher;
import com.dercd.bungeecord.cdlib.tools.minecraft.UUIDFetcher;
import com.google.gson.JsonObject;

import com.dercd.cdio.CloseReason;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class ChatManager implements Listener, Runnable
{
	public SyncedSetMap<Connection, String> dignoreSubscribes = new SyncedSetMap<Connection, String>();
	public SyncedSetMap<Connection, UUID> ignoreSubscribes = new SyncedSetMap<Connection, UUID>();
	public SyncedSetMap<Connection, UUID> silentSubscribes = new SyncedSetMap<Connection, UUID>();
	
	ProfileManager pm;
	NetworkManager networkManager;
	UUIDFetcher uuidFetcher = CDLib.getInstance().getUUIDFetcher();
	NameFetcher nameFetcher = CDLib.getInstance().getNameFetcher();
	MuteManager muteManager;
	BungeeCord bc = BungeeCord.getInstance();
	BungeeScheduler s = this.bc.getScheduler();
	VisibleChecker vc;
	ConnectedChat cc;
	
	private Lock eventsLock = new ReentrantLock();
	private List<ChatEvent> events = new ArrayList<ChatEvent>();
	
	public SyncedSetMap<UUID, UUID> playerIgnores = new SyncedSetMap<UUID, UUID>();
	public SyncedSetMap<UUID, String> playerDIgnores = new SyncedSetMap<UUID, String>();
	FileOutputStream file;
	
	public ChatManager(ConnectedChat cc)
	{
		this.cc = cc;
		this.pm = cc.pm;
		this.networkManager = new NetworkManager(this);
		this.muteManager = new MuteManager(this);
		this.vc = new VisibleChecker(this.networkManager.pokeHandler);
		this.openFileStream();
	}

	@EventHandler
	public void onChat(ChatEvent e)
	{
		ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
		ServerInfo si = pp.getServer().getInfo();
		try
		{
			this.writeFileStream("[" + si.getName() + "] " + pp.getName() + ": " + e.getMessage());
		}
		catch(Exception x) { }
		ServerData sd = this.networkManager.serverData.get(si);
		if(sd == null || sd.isTook() || (sd.isChatTook() && sd.isCmdTook()) || !this.networkManager.isInNetwork(pp))
			return;
		if(sd.hasPlayer(pp.getUniqueId()))
		{
			PlayerData pd = sd.getPlayer(pp.getUniqueId());
			if(pd == null || pd.isTook())
				return;
		}
		if(e.getMessage().startsWith("/"))
		{
			if(sd.isCmdTook() || !checkValid(e.getMessage()))
				return;
		}
		else if(sd.isChatTook())
			return;
		e.setCancelled(true);
		addChat(e);
	}
	private boolean checkValid(String command)
	{
		switch (command.toLowerCase().split((" "))[0])
		{
			case "/msg":
			case "/tell":
			case "/t":
			case "/whisper":
			case "/w":
			case "/r":
			case "/reply":
			case "/ac":
			case "/mute":
			case "/unmute":
			case "/silent":
			case "/msgtoggle":
			case "/wtoggle":
			case "/ignore":
				return true;
			default:
				return false;
		}
	}
	public synchronized void addChat(ChatEvent e)
	{
		this.eventsLock.lock();
		try
		{
			this.events.add(e); notifyAll();
		}
		finally
		{
			this.eventsLock.unlock();
		}
	}
	public void procChatEvent(ChatEvent e) throws IOException
	{
		try
		{
			if (e.getMessage().startsWith("/"))
				doCmd(e.getMessage(), (ProxiedPlayer) e.getSender());
			else
				doChat(e);
		}
		catch (NullPointerException x)
		{
			x.printStackTrace();
		}
	}
	
	protected void doCommand(String args)
	{
		String[] split = args.split(" ");
		switch (split[0])
		{
			case "killall":
				for(Connection c : new HashSet<Connection>(this.networkManager.messageProcessor.connected.values()))
					c.close(CloseReason.MANUAL);
				this.networkManager.messageProcessor.connected.clear();
				System.out.println("All CC-Connections killed");
				return;
			case "kill":
				if(split.length != 2)
					return;
				ServerInfo si = BungeeCord.getInstance().getServerInfo(split[1]);
				if(si == null)
					return;
				Connection c = this.networkManager.messageProcessor.connected.get(si);
				if(c == null)
					return;
				c.close(CloseReason.MANUAL);
				return;
			case "unlock":
				try { this.eventsLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.playerLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.suggestionsLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.sVanishLock.unlock(); }
				catch(Exception x) {}
				try { this.cc.tch.tabLock.unlock(); }
				catch(Exception x) {}
				try { this.cc.tch.counterLock.unlock(); }
				catch(Exception x) {}
				return;
			case "threadreset":
				System.out.println("Cancelling Tasks");
				for(ScheduledTask t : this.cc.threads.values())
					t.cancel();
				System.out.println("Unlocking locks");
				try { this.eventsLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.playerLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.suggestionsLock.unlock(); }
				catch(Exception x) {}
				try { this.networkManager.pokeHandler.sVanishLock.unlock(); }
				catch(Exception x) {}
				try { this.cc.tch.tabLock.unlock(); }
				catch(Exception x) {}
				try { this.cc.tch.counterLock.unlock(); }
				catch(Exception x) {}
				System.out.println("Restarting Tasks");
				BungeeScheduler bs = BungeeCord.getInstance().getScheduler();
				this.cc.threads.put(this.getClass(), bs.runAsync(this.cc, this));
				this.cc.threads.put(this.cc.tch.getClass(), bs.runAsync(this.cc, this.cc.tch));
				this.cc.threads.put(this.networkManager.messageProcessor.getClass(), bs.runAsync(this.cc, this.networkManager.messageProcessor));
				return;
		}
		System.out.println("Unknown Command");
	}

	public void doCmd(String message, ProxiedPlayer sender) throws IOException
	{
		System.out.println(sender.getName() + " issued cc-proxy command: " + message);
		if(message.contains(" "))
			switch (message.toLowerCase().split((" "))[0])
			{
				case "/msg":
				case "/tell":
				case "/t":
				case "/whisper":
				case "/w":
					doMsg(sender, message.substring(message.indexOf(' ') + 1), false);
					return;
				case "/r":
				case "/reply":
					doReply(sender, message.substring(message.indexOf(' ') + 1));
					return;
				case "/ac":
					doAc(sender, message.substring(message.indexOf(' ') + 1));
					return;
				case "/mute":
					doMute(sender, message.substring(message.indexOf(' ') + 1));
					return;
				case "/unmute":
					doUnmute(sender, message.substring(message.indexOf(' ') + 1));
					return;
				case "/ignore":
					doIgnore(sender, message.substring(message.indexOf(' ') + 1));
			}
		else
			switch(message.toLowerCase())
			{
				case "/msgtoggle":
				case "/telltoggle":
				case "/wtoggle":
					doMsgToggle(sender);
					return;
				case "/silent":
					doSilent(sender);
					return;
			}
	}

	private void doIgnore(ProxiedPlayer pp, String msg) throws IOException
	{
		UUID u = pp.getUniqueId();
		String toIgnoreName = msg.split(" ")[0];
		if(disguiseOnline(pp.getServer().getInfo(), toIgnoreName) != null)
		{
			doDIgnore(pp, toIgnoreName);
			return;
		}
		JsonObject jo = new JsonObject();
		PlayerProfile profile = this.pm.getProfile(u);
		if(profile.getDIgnores().remove(toIgnoreName))
		{
			doDUnignore(pp, toIgnoreName);
			return;
		}
		ProxiedPlayer toIgnore = this.bc.getPlayer(toIgnoreName);
		if(toIgnore == null)
		{
			MessageBuffer.send(pp, Messages.playerDisabledMsgOrNotFound);
			return;
		}
		if(pp == toIgnore)
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.GOLD) + "Why do you want to ignore yourself? O_o");
			return;
		}
		UUID toIgnoreUUID = toIgnore.getUniqueId();
		jo.addProperty("action", "PlayerIgnore");
		jo.addProperty("uuid", toIgnoreUUID.toString());
		jo.addProperty("ignorant", u.toString());
		Set<UUID> ignores = profile.getIgnores();
		if(ignores.remove(toIgnoreUUID))
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You are no longer ignoring " + toIgnoreName);
			jo.addProperty("data", false);
		}
		else
		{
			ignores.add(toIgnoreUUID);
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You are now ignoring " + toIgnoreName + " all over Zombfort");
			jo.addProperty("data", true);
		}
		for(Connection c : this.ignoreSubscribes.getByVal(toIgnoreUUID))
			c.send(jo);
	}
	private void doDUnignore(ProxiedPlayer pp, String name) throws IOException
	{
		MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You are no longer ignoring " + name);
		JsonObject jo = new JsonObject();
		jo.addProperty("action", "PlayerDIgnore");
		jo.addProperty("name", name);
		jo.addProperty("ignorant", pp.getUniqueId().toString());
		jo.addProperty("data", false);
		for(Connection c : this.dignoreSubscribes.getByVal(name))
			c.send(jo);
	}
	private void doDIgnore(ProxiedPlayer pp, String name) throws IOException
	{
		UUID u = pp.getUniqueId();
		PlayerProfile profile = this.pm.getProfile(u);
		JsonObject jo = new JsonObject();
		jo.addProperty("action", "PlayerDIgnore");
		jo.addProperty("name", name);
		jo.addProperty("ignorant", u.toString());
		Set<String> dignores = profile.getDIgnores();
		if(dignores.remove(name))
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You are no longer ignoring " + name);
			jo.addProperty("data", false);
		}
		else
		{
			dignores.add(name);
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You are now ignoring " + name + " all over Zombfort");
			jo.addProperty("data", true);
		}
		for(Connection c : this.dignoreSubscribes.getByVal(name))
			c.send(jo);
	}
	private void doSilent(ProxiedPlayer pp) throws IOException
	{
		UUID u = pp.getUniqueId();
		JsonObject jo = new JsonObject();
		PlayerProfile profile = this.pm.getProfile(u);
		jo.addProperty("action", "Silent");
		jo.addProperty("uuid", u.toString());
		if(profile.isSilent())
		{
			profile.setSilent(false);
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "Your chat is no longer silenced");
			jo.addProperty("data", false);
		}
		else
		{
			profile.setSilent(true);
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "Your chat is now silenced");
			jo.addProperty("data", false);
		}
		for(Connection c : this.silentSubscribes.getByVal(u))
			c.send(jo);
	}
	private void doMute(ProxiedPlayer pp, String msg)
	{
		if (!pp.hasPermission("cd.cc.mute")) return;
		String[] args = msg.split(" ");
		if (args.length != 2)
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.RED) + ChatColor.RED + "/mute <player> <time>");
			return;
		}
		String mutedPlayer = args[0];
		UUID uMutedPlayer;
		try
		{
			uMutedPlayer = this.uuidFetcher.fetch(mutedPlayer);
		}
		catch (CDPlayerResolveException x)
		{
			x.handle(pp);
			return;
		}
		int time = Var.unpackTime(args[1]);
		if (time == -1)
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.RED) + ChatColor.RED + "The given time is invalid");
			return;
		}
		this.muteManager.mute(uMutedPlayer, pp.getUniqueId(), time, pp.getServer().getInfo());
	}
	private void doUnmute(ProxiedPlayer pp, String msg)
	{
		if (!pp.hasPermission("cd.cc.mute")) return;
		String[] args = msg.split(" ");
		if (args.length != 1)
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.RED) + ChatColor.RED + "/unmute <player>");
			return;
		}
		String mutedPlayer = args[0];
		UUID uMutedPlayer;
		try
		{
			uMutedPlayer = this.uuidFetcher.fetch(mutedPlayer);
		}
		catch (CDPlayerResolveException x)
		{
			x.handle(pp);
			return;
		}
		if (uMutedPlayer == null)
		{
			new CDPlayerNameNotFoundException().handle(pp);
			return;
		}
		ServerInfo server = pp.getServer().getInfo();
		if (!this.muteManager.isMuted(uMutedPlayer, server))
		{
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.RED) + ChatColor.GOLD + "The player isn't muted here");
			return;
		}
		this.muteManager.unmute(uMutedPlayer, pp.getUniqueId(), pp.getServer().getInfo());
	}
	private void doAc(ProxiedPlayer pp, String msg)
	{
		if(!pp.hasPermission("cd.cc.ac") || !checkNetworks("cd.cc.ac.", pp))
		{
			MessageBuffer.send(pp, ChatColor.DARK_RED + "You don't have the permission to do that");
			return;
		}
		PlayerProfile profile = this.pm.getProfile(pp.getUniqueId());
		if(profile.isSilent())
			MessageBuffer.send(pp, Messages.halfSilentMessage);
		PlayerData pd;
		String displayName;
		msg = replaceColors(msg, pp);
		for (ServerData sd : this.networkManager.getServers(pp))
		{
			if(sd.getServer().getPlayers().size() == 0) continue;
			pd = sd.getPlayer(pp.getUniqueId());
			displayName = (pd.hasDisplayName() ? pd.getDisplayName() : pp.getName());
			sendServerChat(sd.getServer(), "[Team] " + pd.prefix + displayName + pd.suffix + ChatColor.WHITE + ": " + msg);
			BaseComponent[] toSend = MessageBuffer.get(ChatColor.DARK_AQUA + "[" + ChatColor.RED + "Team" + ChatColor.DARK_AQUA + "] " + pd.prefix + displayName + pd.suffix + ChatColor.WHITE + ": " + msg); 
			for (ProxiedPlayer p : sd.getServer().getPlayers())
				if (p.hasPermission("cd.cc.ac") && checkNetworks("cd.cc.ac.", p) && !profile.isSilent()/*!this.silent.contains(p.getUniqueId())*/)
					p.sendMessage(toSend);
		}
	}
	private void doMsg(ProxiedPlayer pp, ProxiedPlayer messagedPlayer, String msg, boolean reply, boolean disguisedResolved)
	{
		PlayerProfile profileMes, profile = this.pm.getProfile(pp.getUniqueId());
		if (messagedPlayer == null || (profileMes = this.pm.getProfile(messagedPlayer.getUniqueId())).isMsgToggled() || !this.networkManager.areSameNetwork(pp, messagedPlayer) || (!reply && !this.vc.check(pp, messagedPlayer)))
		{
			MessageBuffer.send(pp, Messages.playerDisabledMsgOrNotFound);
			return;
		}
		if(profileMes.isSilent())
		{
			MessageBuffer.send(pp, Messages.playerSilenced);
			return;
		}
		String rePlayer = messagedPlayer.getName();
		String frPlayer = pp.getName();
		ServerData sdFrom = this.networkManager.serverData.get(pp.getServer().getInfo());
		ServerData sdTo = this.networkManager.serverData.get(messagedPlayer.getServer().getInfo());
		PlayerData pdFrom1 = sdFrom.getPlayer(pp.getUniqueId());
		PlayerData pdFrom2 = sdFrom.getPlayer(messagedPlayer.getUniqueId());
		PlayerData pdTo1, pdTo2;
		if(sdFrom != sdTo)
		{
			pdTo1 = sdTo.getPlayer(pp.getUniqueId());
			pdTo2 = sdTo.getPlayer(messagedPlayer.getUniqueId());
		}
		else
		{
			pdTo1 = pdFrom1;
			pdTo2 = pdFrom2;
		}
		boolean ignore;
		if(pdTo1.hasDisplayName())
			ignore = profileMes.getDIgnores().contains(pdTo1.getDisplayName());
		else
			ignore = profileMes.getIgnores().contains(pp.getUniqueId());
		if(pdFrom1 == null || pdFrom2 == null || pdTo1 == null || pdTo2 == null)
		{
			MessageBuffer.send(pp, Messages.serverDoesntResponse);
			return;
		}
		if(pdTo2.hasDisplayName() && !disguisedResolved && !reply && !pdTo2.isDMBDisabled() && !sdTo.isDMBDisabled())
		{
			MessageBuffer.send(pp, Messages.playerDisabledMsgOrNotFound);
			return;
		}
		msg = replaceColors(msg, pp);
		String s = ChatColor.AQUA + "[" + ChatColor.RESET + pdFrom1.getPrefix() + (pdFrom1.hasDisplayName() ? pdFrom1.getDisplayName() : frPlayer) + pdFrom1.getSuffix() + ChatColor.RESET + " -> " + ChatColor.RESET + pdFrom2.getPrefix() + (pdFrom2.hasDisplayName() ? pdFrom2.getDisplayName() : rePlayer + pdFrom2.getSuffix()) + ChatColor.RESET + ChatColor.AQUA + "] " + ChatColor.WHITE + msg;
		String serverText;
		boolean mutedHere = this.muteManager.isMutedHere(pp);
		boolean mutedThere = this.muteManager.isMutedThere(pp, messagedPlayer.getServer().getInfo());
		if (mutedHere)
		{
			serverText = ChatColor.RED + "(" + ChatColor.RESET + s + ChatColor.RED + ")";
			MessageBuffer.send(pp, Messages.muteMessage);
		}
		else if (mutedThere)
		{
			serverText = ChatColor.YELLOW + "(" + ChatColor.RESET + s + ChatColor.YELLOW + ")";
			MessageBuffer.send(pp, Messages.msgMuteMessage);
		}
		else
		{
			MessageBuffer.send(pp, s);
			if(ignore)
				serverText = ChatColor.DARK_PURPLE + "(" + ChatColor.RESET + s + ChatColor.DARK_PURPLE + ")";
			else
				serverText = s;
		}
		sendServerChat(pp.getServer().getInfo(), serverText);
		s = ChatColor.AQUA + "[" + ChatColor.RESET + pdTo1.getPrefix() + (pdFrom1.hasDisplayName() ? pdFrom1.getDisplayName() : frPlayer) + pdFrom1.getSuffix() + ChatColor.RESET + " -> " + ChatColor.RESET + pdTo2.getPrefix() + (pdFrom2.hasDisplayName() ? pdFrom2.getDisplayName() : rePlayer + pdFrom2.getSuffix()) + ChatColor.RESET + ChatColor.AQUA + "] " + ChatColor.WHITE + msg;
		if (mutedHere)
			serverText = ChatColor.RED + "(" + ChatColor.RESET + s + ChatColor.RED + ")";
		else if (mutedThere)
			serverText = ChatColor.YELLOW + "(" + ChatColor.RESET + s + ChatColor.YELLOW + ")";
		else
			if(ignore)
				serverText = ChatColor.DARK_PURPLE + "(" + ChatColor.RESET + s + ChatColor.DARK_PURPLE + ")";
			else
			{
				MessageBuffer.send(messagedPlayer, s);
				serverText = s;
				profile.reply = messagedPlayer.getUniqueId();
				profileMes.reply = pp.getUniqueId();
			}
		if (sdFrom.getServer() != sdTo.getServer())
			sendServerChat(sdTo.getServer(), serverText);
	}
	private void doMsg(ProxiedPlayer pp, String msg, boolean reply)
	{
		if (!msg.contains(" ") || msg.endsWith(" "))
		{
			MessageBuffer.send(pp, Messages.syntax);
			return;
		}
		PlayerProfile profile = this.pm.getProfile(pp.getUniqueId());
		if(profile.isSilent())
		{
			MessageBuffer.send(pp, Messages.selfSilencedMsg);
			return;
		}
		if(profile.isMsgToggled())
		{
			MessageBuffer.send(pp, Messages.selfDisabledMsg);
			return;
		}
		String rePlayer = msg.substring(0, msg.indexOf(' '));
		msg = msg.substring(msg.indexOf(' ') + 1);
		PlayerData pd = this.disguiseOnline(pp.getServer().getInfo(), rePlayer);
		ProxiedPlayer messagedPlayer;
		if(pd != null)
			messagedPlayer = this.bc.getPlayer(pd.getUUID());
		else
			messagedPlayer = this.bc.getPlayer(rePlayer);
		if(pp == messagedPlayer)
		{
			MessageBuffer.send(pp, Messages.selfMsg);
			return;
		}
		if(messagedPlayer != null && ((pd == null && profile.getIgnores().contains(messagedPlayer.getUniqueId())) || (pd != null && profile.getDIgnores().contains(rePlayer))))
		{
			MessageBuffer.send(pp, Messages.selfIgnoringMsg);
			return;
		}
		doMsg(pp, messagedPlayer, msg, reply, pd != null);
	}
	private void doReply(ProxiedPlayer pp, String msg)
	{
		PlayerProfile profile = this.pm.getProfile(pp.getUniqueId());
		UUID mUUID = profile.reply;
		ProxiedPlayer replyPlayer;
		if (mUUID == null || (replyPlayer = this.bc.getPlayer(mUUID)) == null || !this.vc.check(pp, replyPlayer))
		{
			MessageBuffer.send(pp, Messages.noReply);
			return;
		}
		doMsg(pp, replyPlayer, msg, true, false);
	}
	private void doMsgToggle(ProxiedPlayer pp)
	{
		PlayerProfile profile = this.pm.getProfile(pp.getUniqueId());
		profile.setMsgToggled(!profile.isMsgToggled());
		if(profile.isMsgToggled())
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "Private messages are now disabled for you");
		else
			MessageBuffer.send(pp, Var.getExclamation(ChatColor.DARK_PURPLE) + "You can receive private messages again");
	}
	
	public void doChat(ChatEvent e)
	{
		if (!(e.getSender() instanceof ProxiedPlayer)) return;
		ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
		PlayerProfile profile = this.pm.getProfile(pp.getUniqueId());
		String message = e.getMessage();
		e.setCancelled(true);
		if (message.startsWith("@") && pp.hasPermission("cd.cc.ac") && checkNetworks("cd.cc.ac.", pp) && message.length() > 1)
		{
			doAc((ProxiedPlayer) e.getSender(), message.substring(1));
			return;
		}
		if (this.muteManager.isMutedHere(pp))
		{
			MessageBuffer.send(pp, Messages.muteMessage);
			for (ServerData sd : this.networkManager.getServers(pp))
				sendServerChat(sd.getServer(), ChatColor.RED + "(" + ChatColor.RESET + getChatMessage(pp.getUniqueId(), message, sd) + ChatColor.RED + ")");
			return;
		}
		Set<ServerInfo> mutedServers = this.muteManager.getConnectedMutedServers(pp);
		if (mutedServers.size() != 0)
		{
			MessageBuffer.send(pp, Messages.halfMuteMessage1);
			MessageBuffer.send(pp, Messages.halfMuteMessage2);
			StringBuilder sb = new StringBuilder(Messages.halfMuteMessage3);
			for (ServerInfo si : mutedServers)
				sb.append(" " + si.getName());
			MessageBuffer.send(pp, sb.toString());
		}
		message = replaceColors(message, pp);
		if(profile.isSilent())
			MessageBuffer.send(pp, Messages.halfSilentMessage);
		for (ServerData sd : this.networkManager.getServers(pp))
			if (!mutedServers.contains(sd.getServer())) sendChat(sd, pp, message);
			else sendServerChat(sd.getServer(), ChatColor.YELLOW + "(" + ChatColor.RESET + getChatMessage(pp.getUniqueId(), message, sd) + ChatColor.YELLOW + ")");
	}

	public void sendChat(ServerData sd, ProxiedPlayer pp, String message)
	{
		sendChat(sd.getServer(), getChatMessage(pp.getUniqueId(), message, sd), pp);
	}

	private void sendChat(ServerInfo si, String text, ProxiedPlayer sender)
	{
		if(text == null) return;
		sendServerChat(si, text);
		ServerData sd = this.networkManager.serverData.get(si);
		PlayerData pd = sd.getPlayer(sender.getUniqueId());
		Set<UUID> ignorants;
		if(!pd.hasDisplayName())
			ignorants = this.playerIgnores.getByVal(sender.getUniqueId());
		else
			ignorants = this.playerDIgnores.getByVal(pd.getDisplayName());
		boolean ignoreFree = ignorants.isEmpty();
		BaseComponent[] toSend = MessageBuffer.get(text);
		for (ProxiedPlayer pp : si.getPlayers())
			if(!this.pm.getProfile(pp.getUniqueId()).isSilent() && (ignoreFree || !ignorants.contains(pp.getUniqueId())))
				pp.sendMessage(toSend);
	}

	void sendServerChat(ServerInfo si, String text)
	{
		try
		{
			JsonObject jo = new JsonObject();
			jo.addProperty("action", "Chat");
			jo.addProperty("data", text);
			Connection c = this.networkManager.messageProcessor.connected.get(si);
			if(c == null) return;
			c.send(jo);
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}
	}

	private String getChatMessage(UUID player, String msg, ServerData sd)
	{
		PlayerData pd = sd.getPlayer(player);
		String prefix = (pd == null ? "" : pd.getPrefix());
		String suffix = (pd == null ? "" : pd.getSuffix());
		String displayName;
		try
		{
			displayName = (pd == null ? this.nameFetcher.fetch(player) : (pd.hasDisplayName() ? pd.getDisplayName() : this.nameFetcher.fetch(player)));			
		}
		catch (CDPlayerResolveException x)
		{
			return null;
		}
		return sd.getChatFormat().replace("%1$s", prefix + displayName + suffix + "§r").replace("%2$s", msg);
	}
	
	public PlayerData disguiseOnline(ServerInfo si, String name)
	{
		try
		{
			for(ServerData sd : this.networkManager.getServers(this.networkManager.serverData.get(si).getNetworks()))
				for(Entry<UUID, PlayerData> e : sd.getPlayers())
					if(name.equals(e.getValue().getDisplayName()))
						return e.getValue();
		}
		catch(Exception x) { }
		return null;
	}
	
	public boolean checkNetworks(String basePermission, ProxiedPlayer pp)
	{
		Set<Network> networks = this.networkManager.getNetworks(pp);
		for(Network n : networks)
			if(!pp.hasPermission(basePermission + n.getId()))
				return false;
		return true;
	}
	private void openFileStream()
	{
		try
		{
			File f = new File("./plugins/BungeeBans");
			if(!f.exists()) return;
			f = new File("./plugins/BungeeBans/log");
			this.file = new FileOutputStream(f, true);
		}
		catch(Exception x) {}
	}
	void closeFileStream()
	{
		try
		{
			this.file.close();
		}
		catch(Exception x) {}
	}
	private void writeFileStream(String text)
	{
		try
		{
			this.file.write((Data.getTime() + " >> " + text + "\n").getBytes());
		}
		catch(Exception x) {}
	}
	
	public String replaceColors(String message, ProxiedPlayer sender)
	{
		if(!checkNetworks("cd.cc.color.", sender)) return message;
		return message.replace("&&", "\r").replace("&", "§").replace("\r", "&");
	}
	
	@Override
	public void run()
	{
		try
		{
			ChatEvent event;
			while(true)
			{
				while(!this.events.isEmpty())
				{
					this.eventsLock.lock();
					event = this.events.remove(0);
					this.eventsLock.unlock();
					procChatEvent(event);
				}
				secWait();
			}
		}
		catch(InterruptedException | IOException x)
		{
			System.out.println("ChatManager-Thread: Interrupted");
		}
	}
	private synchronized void secWait() throws InterruptedException
	{
		if(!this.events.isEmpty()) return;
		wait();
	}
}
