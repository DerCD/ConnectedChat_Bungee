package com.dercd.bungeecord.plugins.connectedchat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.dercd.bungeecord.cdlib.exceptions.CDPlayerResolveException;
import com.dercd.bungeecord.cdlib.tools.Tools.Data;
import com.dercd.bungeecord.cdlib.tools.Tools.MessageBuffer;
import com.dercd.bungeecord.cdlib.tools.Tools.Var;
import com.dercd.bungeecord.cdlib.tools.collection.PresetMap;
import com.dercd.bungeecord.cdlib.tools.minecraft.NameFetcher;
import com.google.gson.JsonObject;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MuteManager
{
	Map<UUID, Map<ServerInfo, Mute>> mutes = new PresetMap<UUID, Map<ServerInfo, Mute>>(new HashMap<ServerInfo, Mute>());
	ChatManager chatManager;
	NameFetcher nameFetcher;
	NetworkManager networkManager;

	public MuteManager(ChatManager chatManager)
	{
		this.chatManager = chatManager;
		this.networkManager = chatManager.networkManager;
		this.nameFetcher = chatManager.nameFetcher;
	}

	public boolean isMutedHere(ProxiedPlayer pp)
	{
		ServerInfo si = pp.getServer().getInfo();
		UUID u = pp.getUniqueId();
		if (this.mutes.get(u) == null) return false;
		refresh(u);
		return this.mutes.get(u).get(si) != null;
	}

	public boolean isMutedThere(ProxiedPlayer pp, ServerInfo si)
	{
		UUID u = pp.getUniqueId();
		if (this.mutes.get(u) == null) return false;
		refresh(u);
		return this.mutes.get(u).get(si) != null;
	}

	public boolean isMuted(UUID user, ServerInfo si)
	{
		if (!this.mutes.containsKey(user)) return false;
		Map<ServerInfo, Mute> map = this.mutes.get(user);
		if (map == null) return false;
		refresh(user);
		return map.containsKey(si);
	}

	public Set<ServerInfo> getConnectedMutedServers(ProxiedPlayer pp)
	{
		UUID u = pp.getUniqueId();
		refresh(u);
		Map<ServerInfo, Mute> playerMutes = this.mutes.get(u);
		Set<ServerInfo> back = new HashSet<ServerInfo>();
		if (playerMutes == null) return back;
		for (ServerData sd : this.chatManager.networkManager.getServers(pp))
			if (playerMutes.get(sd.getServer()) != null) back.add(sd.getServer());
		return back;
	}

	public void refresh(UUID player)
	{
		if (this.mutes.get(player) == null) return;
		Map<ServerInfo, Mute> map = this.mutes.get(player);
		for (ServerInfo si : new HashMap<ServerInfo, Mute>(map).keySet())
			if (map.get(si).isExpired()) map.remove(si);
	}

	public void mute(UUID user, UUID muter, int time, ServerInfo server)
	{
		Mute m = new Mute(time, user, muter, server);
		if (!this.mutes.containsKey(user)) this.mutes.put(user, new HashMap<ServerInfo, Mute>());
		this.mutes.get(user).put(server, m);
		try
		{
			notifyNetworks(m);
		}
		catch (CDPlayerResolveException x)
		{
			x.printStackTrace();
		}
	}

	public boolean unmute(UUID user, UUID unmuter, ServerInfo server)
	{
		if (!isMuted(user, server)) return false;
		Map<ServerInfo, Mute> map = this.mutes.get(user);
		try
		{
			notifyNetworks(map.remove(server), unmuter);
		}
		catch (CDPlayerResolveException x)
		{
			x.printStackTrace();
		}
		return true;
	}

	public void notifyNetworks(Mute m) throws CDPlayerResolveException
	{
		String text = ConnectedChat.mbeg + "Player " + this.nameFetcher.fetch(m.getMuter()) + ChatColor.DARK_GREEN + " muted " + ChatColor.GOLD + this.nameFetcher.fetch(m.getUser()) + ChatColor.DARK_GREEN + " for " + ChatColor.RED + Var.packTime(m.getTime()) + ChatColor.DARK_GREEN + " on Server " + ChatColor.DARK_PURPLE + m.getServer().getName();
		BaseComponent[] tc = MessageBuffer.get(text);
		System.out.println(text);
		for (ProxiedPlayer pp : BungeeCord.getInstance().getPlayer(m.getMuter()).getServer().getInfo().getPlayers())
			if (pp.hasPermission("cd.cc.mute")) pp.sendMessage(tc);
	}

	public void notifyNetworks(Mute m, UUID unmuter) throws CDPlayerResolveException
	{
		String text = ConnectedChat.mbeg + "Player " + this.nameFetcher.fetch(unmuter) + ChatColor.DARK_GREEN + " unmuted " + ChatColor.GOLD + this.nameFetcher.fetch(m.getUser()) + ChatColor.DARK_GREEN + " on Server " + ChatColor.DARK_PURPLE + m.getServer().getName();
		BaseComponent[] tc = MessageBuffer.get(text);
		System.out.println(text);
		for (ProxiedPlayer pp : BungeeCord.getInstance().getPlayer(m.getMuter()).getServer().getInfo().getPlayers())
			if (pp.hasPermission("cd.cc.mute")) pp.sendMessage(tc);
	}


	public static class Mute
	{
		private ServerInfo server;
		private long start;
		private int time;
		private UUID user;
		private UUID muter;

		public Mute(long start, int time, UUID user, UUID muter, ServerInfo server)
		{
			this.start = start;
			this.time = time;
			this.user = user;
			this.server = server;
			this.muter = muter;
		}

		public Mute(int time, UUID user, UUID muter, ServerInfo server)
		{
			this(Data.getTimestamp(), time, user, muter, server);
		}

		public ServerInfo getServer()
		{
			return this.server;
		}

		public boolean isExpired()
		{
			return Data.getTimestamp() - this.start > this.time;
		}

		public long getStart()
		{
			return this.start;
		}

		public int getTime()
		{
			return this.time;
		}

		public UUID getUser()
		{
			return this.user;
		}

		public UUID getMuter()
		{
			return this.muter;
		}
		
		public JsonObject toJson()
		{
			JsonObject base = new JsonObject();
			base.addProperty("server", this.server.getName());
			base.addProperty("start", this.start);
			base.addProperty("time", this.time);
			base.addProperty("user", this.user.toString());
			base.addProperty("muter", this.muter.toString());
			return base;
		}
		
		public static Mute fromJson(JsonObject jo)
		{
			try
			{
				ServerInfo si = BungeeCord.getInstance().getServerInfo(jo.get("server").getAsString());
				long start = jo.get("start").getAsLong();
				int time = jo.get("time").getAsInt();
				UUID user = UUID.fromString(jo.get("user").getAsString());
				UUID muter = UUID.fromString(jo.get("muter").getAsString());
				return new Mute(start, time, user, muter, si);
			}
			catch (Exception x)
			{
				return null;
			}
		}
	}
}
