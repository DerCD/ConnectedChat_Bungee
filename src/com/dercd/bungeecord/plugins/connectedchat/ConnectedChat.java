package com.dercd.bungeecord.plugins.connectedchat;

import java.util.HashMap;
import java.util.Map;

import com.dercd.bungeecord.cdlib.tools.Tools.Var;

import com.dercd.cdio.CDPortListener;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class ConnectedChat extends Plugin
{
	ProfileManager pm = new ProfileManager(this);
	ChatManager cm = new ChatManager(this);
	TabCompleteHelper tch = new TabCompleteHelper(this);
	public CDPortListener portListener;
	public static String mbeg = ChatColor.AQUA + "[ConnectedChat] " + ChatColor.DARK_GREEN;
	Map<Class<?>, ScheduledTask> threads = new HashMap<Class<?>, ScheduledTask>();
	
	@Override
	public void onEnable()
	{
		BungeeCord bc = BungeeCord.getInstance();
		BungeeScheduler bs = bc.getScheduler();
		this.threads.put(this.cm.getClass(), bs.runAsync(this, this.cm));
		this.threads.put(this.tch.getClass(), bs.runAsync(this, this.tch));
		this.threads.put(this.cm.networkManager.messageProcessor.getClass(), bs.runAsync(this, this.cm.networkManager.messageProcessor));
		bc.getPluginManager().registerListener(this, this.cm);
		bc.getPluginManager().registerListener(this, this.tch);
		bc.getPluginManager().registerListener(this, this.cm.networkManager.messageProcessor);
		bc.getPluginManager().registerCommand(this, new Command("cc", "cd.cc")
		{
			@Override
			public void execute(CommandSender cs, String[] args)
			{
				System.out.println(cs.getName() + " issued proxy command: " + Var.arrToString(args, 0));
				ConnectedChat.this.cm.doCommand(Var.arrToString(args, 0));
			}
		});
		bc.getPluginManager().registerCommand(this, new Command("cc", "cd.cc")
		{
			@Override
			public void execute(CommandSender cs, String[] args)
			{
				System.out.println(cs.getName() + " issued proxy command: " + Var.arrToString(args, 0));
				ConnectedChat.this.cm.doCommand(Var.arrToString(args, 0));
			}
		});
		this.pm.loadUserdata();
		this.portListener = new CDPortListener(23457);
		this.portListener.keyWordIn = Connection.keyWord.getBytes();
		this.portListener.out = (s) -> { System.out.println("[ConnectedChat] " + s); };
		this.portListener.creator = (socket, keyWordIn, keyWordOut) ->
		{
			try
			{
				return new Connection(socket, this.cm.networkManager.messageProcessor);
			}
			catch (Exception x)
			{
				x.printStackTrace();
				return null;
			}
		};
		bs.runAsync(this, () ->
		{
			this.portListener.t = Thread.currentThread();
			this.portListener.run();
		});
	}
	
	@Override
	public void onDisable()
	{
		this.pm.saveUserdata();
		this.cm.closeFileStream();
	}
	
	public ChatManager getChatManager()
	{
		return this.cm;
	}
	
	public ProfileManager getProfileManager()
	{
		return this.pm;
	}
	
	public TabCompleteHelper getTabCompleteHelper()
	{
		return this.tch;
	}
}
