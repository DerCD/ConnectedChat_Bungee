package com.dercd.bungeecord.plugins.connectedchat;

import java.util.UUID;

public class PlayerData
{
	String prefix, suffix;
	final UUID uuid;
	private String displayName;
	private boolean took = false;
	private boolean disabledDMB = false;
	
	
	public PlayerData(UUID uuid, String prefix, String suffix)
	{
		this.prefix = prefix == null ? "" : prefix;
		this.suffix = suffix == null ? "" : suffix;
		this.uuid = uuid;
	}

	public void setDisplayName(String name)
	{
		this.displayName = name;
	}
	public boolean hasDisplayName()
	{
		return this.displayName != null;
	}
	public void removeDisplayName()
	{
		setDisplayName(null);
	}
	public String getDisplayName()
	{
		return this.displayName;
	}

	public boolean isTook()
	{
		return this.took;
	}
	public void setTook(boolean b)
	{
		this.took = b;
	}
	
	public boolean isDMBDisabled()
	{
		return this.disabledDMB;
	}
	public void setDMBDisabled(boolean b)
	{
		this.disabledDMB = b;
	}
	
	
	public UUID getUUID()
	{
		return this.uuid;
	}
	public String getPrefix()
	{
		return this.prefix;
	}
	public String getSuffix()
	{
		return this.suffix;
	}
}
