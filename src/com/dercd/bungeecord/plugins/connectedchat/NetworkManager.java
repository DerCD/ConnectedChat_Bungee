package com.dercd.bungeecord.plugins.connectedchat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class NetworkManager
{
	public ChatManager chatManager;
	Map<String, Network> networks = new HashMap<String, Network>();
	public Map<ServerInfo, ServerData> serverData = new HashMap<ServerInfo, ServerData>();
	public PokeHandler pokeHandler;
	public MessageProcessor messageProcessor;
	BungeeCord bc = BungeeCord.getInstance();

	public NetworkManager(ChatManager chatManager)
	{
		this.chatManager = chatManager;
		this.pokeHandler = new PokeHandler(this, chatManager.cc);
		this.messageProcessor = new MessageProcessor(this.pokeHandler, this);
	}

	public void addNetwork(Network network)
	{
		String nID = network.getId();
		if (this.networks.containsKey(nID)) removeNetwork(this.networks.get(nID));
		this.networks.put(nID, network);
		for (ServerData sd : network.getServers())
			sd.addNetwork(network);
	}

	public void removeNetwork(Network network)
	{
		if (network == null)
		{
			this.networks.remove(network);
			return;
		}
		for (ServerData sd : network.getServers())
		{
			sd.removeNetwork(network);
		}
		this.networks.remove(network.getId());
	}

	public void removeServer(ServerInfo si)
	{
		ServerData sd = this.serverData.remove(si);
		if(sd == null) return;
		for (Network n : new HashSet<Network>(sd.getNetworks()))
			n.removeServer(sd);
	}

	public boolean areSameNetwork(ProxiedPlayer pp1, ProxiedPlayer pp2)
	{
		ServerData sd1 = this.serverData.get(pp1.getServer().getInfo());
		ServerData sd2 = this.serverData.get(pp2.getServer().getInfo());
		if(sd1 == null || sd2 == null)
			return false;
		Set<Network> l1 = sd1.getNetworks();
		Set<Network> l2 = sd2.getNetworks();
		for(Network n : l1)
			if (l2.contains(n))
				return true;
		return false;
	}

	public boolean isInNetwork(ProxiedPlayer pp)
	{
		return this.serverData.get(pp.getServer().getInfo()) != null;
	}

	public Set<Network> getNetworks(ProxiedPlayer pp)
	{
		ServerData sd = this.serverData.get(pp.getServer().getInfo());
		if(sd == null)
			return null;
		return new HashSet<Network>(sd.getNetworks());
	}

	public void clearAll()
	{
		this.networks.clear();
		this.serverData.clear();
	}

	
	public List<ServerData> getServers(Collection<Network> networks)
	{
		List<ServerData> list = new ArrayList<ServerData>();
		for (Network n : networks)
			for (ServerData sd : n.getServers())
				if (!list.contains(sd))
					list.add(sd);
		return list;
	}
	
	public List<ServerData> getServers(ProxiedPlayer pp)
	{
		return getServers(getNetworks(pp));
	}
}
