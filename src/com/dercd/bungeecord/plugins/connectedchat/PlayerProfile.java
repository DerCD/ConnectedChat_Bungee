package com.dercd.bungeecord.plugins.connectedchat;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.dercd.bungeecord.plugins.connectedchat.MuteManager.Mute;
import net.md_5.bungee.api.config.ServerInfo;

public class PlayerProfile
{
	private boolean
	msgToggle = false,
	silent = false;
	
	public UUID reply;
	
	private Map<ServerInfo, Mute> mutes = new HashMap<ServerInfo, Mute>();
	private Set<UUID> ignores;
	private Set<String> dignores;
	
	public final UUID uuid;
	
	public PlayerProfile(UUID uuid, ChatManager cm)
	{
		this.uuid = uuid;
		this.ignores = cm.playerIgnores.getByKey(uuid);
		this.dignores = cm.playerDIgnores.getByKey(uuid);
	}
	
	public UUID getUUID()
	{
		return this.uuid;
	}
	
	public boolean isMsgToggled()
	{
		return this.msgToggle;
	}
	public boolean isSilent()
	{
		return this.silent;
	}
	public Map<ServerInfo, Mute> getMutes()
	{
		return this.mutes;
	}
	public Set<UUID> getIgnores()
	{
		return this.ignores;
	}
	public Set<String> getDIgnores()
	{
		return this.dignores;
	}
	
	public void setMsgToggled(boolean b)
	{
		this.msgToggle = b;
	}
	public void setSilent(boolean b)
	{
		this.silent = b;
	}
	public void setMutes(Map<ServerInfo, Mute> m)
	{
		this.mutes = m;
	}
	public void setIgnores(Set<UUID> s)
	{
		this.ignores = s;
	}
	public void setDIgnores(Set<String> s)
	{
		this.dignores = s;
	}
}
