package com.dercd.bungeecord.plugins.connectedchat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dercd.bungeecord.cdlib.tools.collection.CounterMap;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.protocol.packet.TabCompleteResponse;
import net.md_5.bungee.scheduler.BungeeScheduler;

public class TabCompleteHelper implements Listener, Runnable
{
	ConnectedChat cc;
	ProfileManager pm;
	NetworkManager nm;
	ChatManager cm;
	PokeHandler pokeHandler;
	BungeeCord bc;
	BungeeScheduler bs;
	
	Lock tabLock = new ReentrantLock();
	private List<TabCompleteEvent> tcRequests = new ArrayList<TabCompleteEvent>();
	Lock counterLock = new ReentrantLock();
	private CounterMap<UUID> counter = new CounterMap<UUID>();
	
	public TabCompleteHelper(ConnectedChat cc)
	{
		this.cc = cc;
		this.pm = cc.getProfileManager();
		this.cm = cc.cm;
		this.nm = cc.cm.networkManager;
		this.pokeHandler = cc.cm.networkManager.pokeHandler;
		this.bc = BungeeCord.getInstance();
		this.bs = this.bc.getScheduler();
	}
	
	//TODO Reenable
	//Deactivated because Bungee throws an error and kicks player
	@EventHandler
	public void onTabComplete(TabCompleteEvent e)
	{
		if(!(e.getSender() instanceof ProxiedPlayer))
			return;
		ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
		ServerData sd = this.cc.cm.networkManager.serverData.get(pp.getServer().getInfo());
		if(sd == null || sd.isTook() || sd.isTabTook() || !this.nm.isInNetwork(pp))
			return;
		if(sd.hasPlayer(pp.getUniqueId()))
		{
			PlayerData pd = sd.getPlayer(pp.getUniqueId());
			if(pd == null || pd.isTook())
				return;
		}
		e.setCancelled(true);
		this.counterLock.lock();
		try
		{
			this.counter.increment(pp.getUniqueId());
		}
		finally
		{
			this.counterLock.unlock();
		}
		addTCEvent(e);
	}
	public synchronized void addTCEvent(TabCompleteEvent e)
	{
		this.tabLock.lock();
		try
		{
			this.tcRequests.add(e);
			notifyAll();
		}
		finally
		{
			this.tabLock.unlock();
		}
	}
	
	
	public void procTabEvent(TabCompleteEvent e)
	{
		ProxiedPlayer pp = (ProxiedPlayer) e.getSender();
		UUID u = pp.getUniqueId();
		this.counterLock.lock();
		try
		{
			Integer i = this.counter.decrement(u);
			if(i < 0)
				this.counter.reset(u);
			else if(i > 1)
				return;
		}
		finally
		{
			this.counterLock.unlock();
		}
		Set<UUID> suggestions = new HashSet<UUID>();
		String input = e.getCursor();
		if(input.contains(" "))
			input = input.substring(input.lastIndexOf(' ') + 1);
		for(ServerData sd : this.nm.getServers(pp))
			suggestions.addAll(this.pokeHandler.getSuggestionsSync(u, sd.getServer(), input));
		ServerData sd = this.nm.serverData.get(pp.getServer().getInfo());
		removeIgnores(u, sd, suggestions);
		List<String> strSuggestions = resolveUUIDs(suggestions, sd);
		TabCompleteResponse tcr = new TabCompleteResponse();
		tcr.setCommands(strSuggestions);
		sendResponse(tcr, pp);
	}
	public List<String> resolveUUIDs(Collection<UUID> uuids, ServerData sd)
	{
		List<String> names = new ArrayList<String>();
		ProxiedPlayer pp;
		PlayerData pd;
		for(UUID u : uuids)
			if((pp = this.bc.getPlayer(u)) != null)
				if(sd.hasPlayer(u) && (pd = sd.getPlayer(u)).hasDisplayName()) names.add(pd.getDisplayName());
				else names.add(pp.getName());
		return names;
	}
	public void removeIgnores(UUID player, ServerData sd, Collection<UUID> players)
	{
		Iterator<UUID> iPlayers = players.iterator();
		Set<String> dignores = this.cm.playerDIgnores.getByKey(player);
		Set<UUID> ignores = this.cm.playerIgnores.getByKey(player);
		PlayerData pd;
		UUID u;
		while(iPlayers.hasNext())
			if(sd.hasPlayer((u = iPlayers.next())) && (pd = sd.getPlayer(u)).hasDisplayName())
			{
				if(dignores.contains(pd.getDisplayName()))
					iPlayers.remove();
				continue;
			}
			else if(ignores.contains(u))
				iPlayers.remove();
	}
	public void sendResponse(TabCompleteResponse tcr, ProxiedPlayer pp)
	{
		this.bs.schedule(this.cc, () -> { pp.unsafe().sendPacket(tcr); }, 0, TimeUnit.NANOSECONDS);
	}
	
	@Override
	public void run()
	{
		try
		{
			TabCompleteEvent event;
			while(true)
			{
				while(!this.tcRequests.isEmpty())
				{
					this.tabLock.lock();
					event = this.tcRequests.remove(0);
					this.tabLock.unlock();
					try
					{
						procTabEvent(event);
					}
					catch(Exception x)
					{
						x.printStackTrace();
					}
				}
				secWait();
			}
		}
		catch(InterruptedException x)
		{
			System.out.println("TabComplete-Thread: Interrupted");
		}
	}
	private synchronized void secWait() throws InterruptedException
	{
		if(!this.tcRequests.isEmpty()) return;
		wait();
	}
}
