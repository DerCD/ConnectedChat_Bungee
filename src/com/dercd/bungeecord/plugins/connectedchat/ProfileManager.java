package com.dercd.bungeecord.plugins.connectedchat;

import java.io.File;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import com.dercd.bungeecord.plugins.connectedchat.MuteManager.Mute;
import net.md_5.bungee.api.config.ServerInfo;

import com.dercd.bungeecord.cdlib.tools.Tools.Data;
import com.dercd.bungeecord.cdlib.tools.collection.PresetMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class ProfileManager
{
	private Map<UUID, PlayerProfile> profiles = new HashMap<UUID, PlayerProfile>();
	public ConnectedChat cc;
	private Map<UUID, Integer> fileHashes = new PresetMap<UUID, Integer>(0);
	
	public ProfileManager(ConnectedChat cc)
	{
		this.cc = cc;
	}
	
	
	public void saveUserdata()
	{
		System.out.println("ConnectedChat: Saving userdata");
		JsonObject base;
		JsonArray arr;
		JsonObject obj;
		File folder = new File("./plugins/ConnectedChat/userdata");
		if(!folder.exists()) folder.mkdirs();
		File playerFile;
		PrintStream ps = null;
		String toWrite;
		PlayerProfile pp;
		UUID u = null;
		for(Entry<UUID, PlayerProfile> e : this.profiles.entrySet())
		{
			try
			{
				pp = e.getValue();
				u = e.getKey();
				base = new JsonObject();
				base.addProperty("silent", pp.isSilent());
				base.addProperty("msgToggle", pp.isMsgToggled());
				arr = new JsonArray();
				for(UUID ignored : pp.getIgnores())
					arr.add(new JsonPrimitive(ignored.toString()));
				base.add("ignores", arr);
				arr = new JsonArray();
				for(String dignored : pp.getDIgnores())
					arr.add(new JsonPrimitive(dignored));
				base.add("dignores", arr);
				arr = new JsonArray();
				for(Entry<ServerInfo, Mute> entry : pp.getMutes().entrySet())
				{
					obj = entry.getValue().toJson();
					obj.remove("user");
					arr.add(obj);
				}
				base.add("mutes", arr);
				if((toWrite = base.toString()).hashCode() == this.fileHashes.get(u)) continue;
				playerFile = new File("./plugins/ConnectedChat/userdata/" + u.toString() + ".json");
				if(playerFile.exists()) playerFile.delete();
				playerFile.createNewFile();
				ps = new PrintStream(playerFile, "UTF-8");
				ps.println(toWrite);
			}
			catch (Exception x)
			{
				System.err.println("ConnectedChat: Error while writing userdata for " + u.toString());
				x.printStackTrace();
			}
			finally
			{
				if(ps != null)
					ps.close();
			}
		}
	}
	public void loadUserdata()
	{
		File folder = new File("./plugins/ConnectedChat/userdata");
		if(!folder.exists())
		{
			System.out.println("ConnectedChat: Couldn't load userdata");
			return;
		}
		System.out.println("ConnectedChat: Loading userdata");
		JsonParser parser = new JsonParser();
		JsonObject base, obj;
		JsonArray arr;
		UUID u;
		Mute m;
		Map<ServerInfo, Mute> mutes;
		Charset utf8 = Charset.forName("UTF-8");
		PlayerProfile pp;
		Set<UUID> ignores;
		Set<String> dignores;
		for(File f : folder.listFiles((dir, name) -> name.endsWith(".json") && name.length() == 41))
		{
			try
			{
				try { u = UUID.fromString(f.getName().substring(0, 36)); }
				catch (IllegalArgumentException x) { continue; }
				pp = new PlayerProfile(u, this.cc.cm);
				base = (JsonObject) parser.parse(Data.readFile(f.getAbsolutePath(), utf8));
				this.fileHashes.put(u, base.toString().hashCode());
				if(base.get("silent").getAsBoolean()) pp.setSilent(true);
				if(base.get("msgToggle").getAsBoolean()) pp.setMsgToggled(true);
				arr = base.get("ignores").getAsJsonArray();
				ignores = pp.getIgnores();
				for(JsonElement je : arr)
					ignores.add(UUID.fromString(je.getAsString()));
				if(base.has("dignores"))
				{
					arr = base.get("dignores").getAsJsonArray();
					dignores = pp.getDIgnores();
					for(JsonElement je : arr)
						dignores.add(je.getAsString());
				}
				arr = base.get("mutes").getAsJsonArray();
				mutes = pp.getMutes();
				for(JsonElement je : arr)
				{
					(obj = je.getAsJsonObject()).addProperty("user", u.toString());
					m = Mute.fromJson(obj);
					mutes.put(m.getServer(), m);
				}
				putProfile(pp);
			}
			catch (Exception x)
			{
				System.err.println("ConnectedChat: Error while reading file " + f.getName());
				x.printStackTrace();
			}
		}
	}
	
	
	public void putProfile(PlayerProfile pp)
	{
		if(pp.getUUID() == null)
			throw new IllegalArgumentException("Players UUID have not to be null");
		this.profiles.put(pp.getUUID(), pp);
	}
	public PlayerProfile getProfile(UUID u)
	{
		PlayerProfile pp = this.profiles.get(u);
		if(pp == null)
			this.profiles.put(u, (pp = new PlayerProfile(u, this.cc.cm)));
		return pp;
	}
}
