package com.dercd.bungeecord.plugins.connectedchat;

import com.dercd.bungeecord.cdlib.tools.Tools.Var;

import net.md_5.bungee.api.ChatColor;

class Messages
{
	public static final String
	playerNotFound = ChatColor.RED + "The requested Player wasn't found",
	playerDisabledMsg = ChatColor.RED + "The requested Player has disabled private messages",
	playerDisabledMsgOrNotFound = ChatColor.RED + "The requested Player wasn't found or has disabled private messages",
	playerSilenced = ChatColor.GOLD + "The requested Player has silenced his chat",
	serverDoesntResponse = ChatColor.DARK_RED + "Error while sending private message: The server doesn't response",
	selfSilencedMsg = ChatColor.RED + "You've silenced your chat. There is no sense in writing private messages",
	selfDisabledMsg = ChatColor.RED + "You've disabled private messages. There is no sense in writing it",
	syntax = ChatColor.RED + "/msg <player> <message>",
	selfIgnoringMsg = ChatColor.RED + "You are ignoring this user. There is no sense in writing private messages to him/her",
	noReply = ChatColor.RED + "There is nobody you could reply",
	selfMsg = ChatColor.RED + "There is no sense in writing private messages to yourself",
	muteMessage = ChatColor.RED + "You're muted here",
	msgMuteMessage = ChatColor.RED + "The player you want to send a message to is on a server you're muted on",
	halfMuteMessage1 = Var.getExclamation(ChatColor.RED) + ChatColor.GOLD + "You aren't muted here but on some connected Servers you are.",
	halfMuteMessage2 = Var.getExclamation(ChatColor.RED) + ChatColor.GOLD + "So your message won't be sent to this servers",
	halfMuteMessage3 = Var.getExclamation(ChatColor.RED) + ChatColor.GOLD + "The affected servers are:" + ChatColor.DARK_PURPLE,
	halfSilentMessage = Var.getExclamation(ChatColor.RED) + ChatColor.WHITE + "Attention: Your message is sent, but you won't see it because you silenced your chat",
	silentRefreshMessage = Var.getExclamation(ChatColor.RED) + ChatColor.RED + "It may take some seconds to refresh it on all servers";
}