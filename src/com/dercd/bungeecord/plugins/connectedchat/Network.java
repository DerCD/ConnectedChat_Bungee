package com.dercd.bungeecord.plugins.connectedchat;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Network
{
	private String id;
	private Map<String, ServerData> connectedServers;
	NetworkManager networkManager;

	public Network(NetworkManager nManager, String id)
	{
		this.id = id;
		this.connectedServers = new HashMap<String, ServerData>();
		this.networkManager = nManager;
	}
	public Network(NetworkManager nManager, String id, List<ServerData> servers)
	{
		this.id = id;
		this.connectedServers = new HashMap<String, ServerData>();
		for (ServerData sd : servers)
			addServer(sd);
		this.networkManager = nManager;
	}

	public Collection<ServerData> getServers()
	{
		return this.connectedServers.values();
	}

	public Network(Network cn)
	{
		this.id = cn.id;
		this.connectedServers = cn.connectedServers;
	}

	public String getId()
	{
		return this.id;
	}

	public boolean isEmpty()
	{
		return this.connectedServers.size() == 0;
	}

	public void addServer(ServerData sd)
	{
		this.connectedServers.put(sd.getName(), sd);
		sd.addNetwork(this);
	}

	public ServerData getServer(String name)
	{
		return this.connectedServers.get(name);
	}

	public void removeServer(String name)
	{
		removeServer(this.connectedServers.remove(name));
	}
	
	public void removeServer(ServerData sd)
	{
		if (sd == null) return;
		sd.removeNetwork(this);
		this.connectedServers.remove(sd.getName());
	}
}
